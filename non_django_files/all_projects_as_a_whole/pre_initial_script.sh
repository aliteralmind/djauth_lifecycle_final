#!/bin/bash
number="\$number"
path="/home/jeffy/django_files/djauth_lifecycle_tutorial/part_$number"
echo $path
echo "What is \$number?"
read number
path="/home/jeffy/django_files/djauth_lifecycle_tutorial/part_$number"
mkdir -p $path
cd $path
touch initial_script.sh
chmod 774 initial_script.sh
echo "Ready to populate ${path}/initial_script.sh"