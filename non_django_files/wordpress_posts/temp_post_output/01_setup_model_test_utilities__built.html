This tutorial demonstrates the entire user-authentication lifecycle in Django, with <a href="https://docs.djangoproject.com/en/1.7/topics/testing/">testing</a> at every step:

<ol>
   <li>Create an account, both with and without email confirmation</li>
   <li>Login, including a forgot-my-password reset (via email), and a keep-me-logged-in checkbox</li>
   <li>A page viewable when logged out, but containing extra information when logged-in</li>
   <li>A page viewable only when logged in: Your user "profile".</li>
   <li>Change your password</li>
   <li>Logout</li>
   <li>Delete your account</li>
</ol>

<i><code><b>[TOC: <a href="/2014/10/16/django_auth_tutorial/">one</a>, <a href="/2014/10/19/django_auth_tutorial_2/">two</a>, <a href="/2014/10/19/django_auth_testing_tutorial_3/">three</a>, <a href="/2014/12/29/django_auth_tutorial_4/">four</a>, <a href="/2015/01/26/django_auth_tutorial_5/">five</a>, <a href="/2015/02/12/django_auth_tutorial_6/">six</a>, <a href="/2015/02/13/django_auth_tutorial_7/">seven</a>, eight, nine, ten]</b></code></i>

The goal of this tutorial is to require <i>as trivial a website as possible</i>, before proceeding onto authentication. In contrast, the How To Tango With Django tutorial does not address authentication until <a href="http://www.tangowithdjango.com/book/chapters/login.html">chapter eight</a>, and Mike Hibbert's video tutorial doesn't talk about it until <a href="https://www.youtube.com/watch?v=CFypO_LNmcc&amp;index=9&amp;list=PLxxA5z-8B2xk4szCgFmgonNcCboyNneMD">chapter nine</a>. It's not possible to start in the middle, as they both expect that all previous steps were followed. (<i><a href="https://code.djangoproject.com/wiki/Tutorials">Other available tutorials</a>.</i>)

<i>Warnings:<ul>
   <li>The testing code increases the amount of code, and the number of steps in this tutorial, by a lot. So creating the demo website is more substantial than I just implied. However, if you did skip the testing--which you shouldn't--implementing the website itself would be much faster. Were proper testing in either the Tango or Hibbert tutorials, they would be a whole lot longer.</li>
   <li>The one type of testing I can't demonstrate in this tutorial is end-user simulation/integration testing with <a href="http://www.seleniumhq.org/">Selenium</a>. My webserver is text-only. This leaves the JavaScript portions of this tutorial, such as client-side make-sure-the-passwords-match verification, untested.</li>
</ul></i><a name="screenshots"></a>

<h1>The trivial website: Screenshots</h1>

Before doing authentication, we need to create <i>something</i>. Let's take a look at that something before actually building it. It will contain these two simple views:

<img src="http://i.imgur.com/WSmtami.jpg" alt="screenshot">

The main &quot;aggregate&quot; page which is publicly viewable, but contains extra information for logged-in users, including a link to their private profile page:

<img src="http://i.imgur.com/psArNo6.jpg" alt="screenshot">

The profile page, which displays every non-password field in the <a href="https://docs.djangoproject.com/en/1.7/topics/auth/default/#user-objects">User model</a>, plus the one extra field that makes up the entirety of <a href="#model">our model</a>: birth year.<a name="install"></a>

<h1>Installation</h1>

Here is <a href="/2014/09/21/jquery_django_tutorial/#my_setup">my setup</a>. The only differences for this tutorial are:

<ul>
   <li>The virtualenv base directory is
     &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_venv/</code></li>
   <li>The project base directory is
     &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_root/</code></li>
</ul>

The reason for this structure is so that the entire <code>django_auth_lifecycle</code> directory can be a Git repository. It is also where I place non-Django files, such as some scripts, some personal-only files, the wordpress posts, including their <a href="/2015/01/03/xbnjblogpostbuilder/">java builders</a>. In actuality, each post is its own repository. When one part is done--or if there's a bug--all changes are copied over to all future parts. Attempting to have all posts in one monster repository is impossible, given my current beginner-level Git skills.

The steps I took, which you will need to tailor to your environment:<ol>
  <li><code>mkdir -p /home/myname/django_auth_lifecycle/djauth_venv/</code></li>
  <li><code>sudo virtualenv -p /usr/bin/python3.4 /home/myname/django_auth_lifecycle/djauth_venv/</code></li>
  <li><code>source /home/myname/django_auth_lifecycle/djauth_venv/bin/activate</code></li>
  <li><code>sudo /home/myname/django_auth_lifecycle/djauth_venv/bin/pip install django</code></li>
  <li><code>sudo /home/myname/django_auth_lifecycle/djauth_venv/bin/pip install gunicorn</code></li>
  <li><code>sudo /home/myname/django_auth_lifecycle/djauth_venv/bin/pip install psycopg2</code> &nbsp; &nbsp; <i>(this step has a lot of output)</i></li>
  <li><code>sudo chown -R myname /home/myname/django_auth_lifecycle/</code></li>
</ol><a name="install_django"></a>

<h1>Install a new Django project</h1>

<ol>
   <li>Start your <code>virtualenv</code>:
     &nbsp; &nbsp; <code>source /home/myname/django_auth_lifecycle/djauth_venv/bin/<a href="https://virtualenv.pypa.io/en/latest/userguide.html#activate-script">activate</a></code> &nbsp; &nbsp; <i>(exit it with <code>deactivate</code>)</i></li>
   <li>Create the project directory:
     &nbsp; &nbsp; <code>mkdir /home/myname/django_auth_lifecycle/djauth_root/</code></li>
   <li>Create the project (this is a long command that belongs on a single line) :
     &nbsp; &nbsp; <code>django-admin.py <a href="https://docs.djangoproject.com/en/1.7/ref/django-admin/#startproject-projectname-destination">startproject</a> django_auth_lifecycle /home/myname/django_auth_lifecycle/djauth_root</code>
   <li>Create the sub-application:<ol>
      <li><code>cd /home/myname/django_auth_lifecycle/djauth_root/</code></li>
      <li><code>python manage.py <a href="https://docs.djangoproject.com/en/1.7/ref/django-admin/#startapp-app-label-destination">startapp</a> auth_lifecycle</code></li>
    </ol>
    <br />&nbsp;
    This and the previous command create the following (items unused by this tutorial are omitted):

<pre>$ tree /home/myname/django_auth_lifecycle/djauth_root/
+-- auth_lifecycle
|   +-- admin.py
|   +-- models.py
|   +-- views.py
+-- django_auth_lifecycle
|   +-- settings.py
|   +-- urls.py
+-- manage.py</pre></li>
   <li>In
     &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_root/django_auth_lifecycle/settings.py</code>
   <ol>
      <li>Add <code>'auth_lifecycle'</code> to <a href="https://docs.djangoproject.com/en/1.7/ref/settings/#std:setting-INSTALLED_APPS"><code>INSTALLED_APPS</code></a></li>
      <li>Configure your <a href="https://docs.djangoproject.com/en/1.7/ref/settings/#databases">database</a> by overwriting the current value with
[code language="java"]DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'database_name_here',
        'USER': 'database_username_here',
        'PASSWORD': 'database_user_password_goes_here',
        'HOST': "localhost",  # Empty for localhost through domain sockets or
                              # '127.0.0.1' for localhost through TCP.
        'PORT': '',           # Set to empty string for default.
    }
}[/code]</li>
     </ol></li>
   <li>If you were not yet prompted to create a superuser, do it now:<ol>
      <li><code>cd /home/myname/django_auth_lifecycle/djauth_root/</code></li>
      <li><code>python manage.py <a href="https://docs.djangoproject.com/en/1.7/ref/django-admin/#django-admin-createsuperuser">createsuperuser</a></code></li>
     </ol>The rest of this tutorial expects the superuser's username and password to both be <code>"admin"</code></li>
</ol><a name="model"></a>

<h1>The model</h1>

The only thing in our model is the user's year-of-birth, which will be stored in a <code>UserProfile</code> model that is linked to the <a href="https://docs.djangoproject.com/en/1.7/topics/auth/default/#user-objects">default Django <code>User</code> model</a>. Although I've added in some validation, a simpler alternative without it is below.

Replace the contents of
 &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_root/auth_lifecycle/models.py</code>
with

[code language="python"]"""
Defines a single extra user-profile field for the user-authentication
lifecycle demo project:
    - Birth year, which must be between <link to MIN_BIRTH_YEAR> and
   <link to MAX_BIRTH_YEAR>, inclusive.
"""
from datetime                   import datetime
from django.contrib.auth.models import User
from django.core.exceptions     import ValidationError
from django.db                  import models

OLDEST_EVER_AGE     = 127  #:Equal to `127`
YOUNGEST_ALLOWED_IN_SYSTEM_AGE = 13   #:Equal to `13`
MAX_BIRTH_YEAR      = datetime.now().year - YOUNGEST_ALLOWED_IN_SYSTEM_AGE
"""Most recent allowed birth year for (youngest) users."""
MIN_BIRTH_YEAR      = datetime.now().year - OLDEST_EVER_AGE
"""Most distant allowed birth year for (oldest) users."""

def _validate_birth_year(birth_year_str):
    """Validator for <link to UserProfile.birth_year>, ensuring the
        selected year is between <link to OLDEST_EVER_AGE> and
        <link to MAX_BIRTH_YEAR>, inclusive.
        Raises:
            ValidationError: When the selected year is invalid.

        - https://docs.djangoproject.com/en/1.7/ref/validators/

        I am a recovered Hungarian Notation junkie (I come from Java). I
        stopped using it long before I started with Python. In this
        particular function, however, because of the necessary cast, it's
        appropriate.
    """
    birth_year_int = -1
    try:
        birth_year_int = int(str(birth_year_str).strip())
    except TypeError:
        raise ValidationError(u'"{0}" is not an integer'.format(birth_year_str))

    if  not (MIN_BIRTH_YEAR <= birth_year_int <= MAX_BIRTH_YEAR):
        message = (u'{0} is an invalid birth year.'
                   u'Must be between {1} and {2}, inclusive')
        raise ValidationError(message.format(
            birth_year_str, MIN_BIRTH_YEAR, MAX_BIRTH_YEAR))
    #It's all good.

class UserProfile(models.Model):
    """Extra information about a user: Birth year.

    ---NOTES---

    Useful related SQL:
    - `select id from auth_user where username <> 'admin';`
    - `select * from auth_lifecycle_userprofile where user_id=(x,x,...);`
    """
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User, related_name="profile")

    # The additional attributes we wish to include.
    birth_year = models.IntegerField(
        blank=True,
        verbose_name="Year you were born",
        validators=[_validate_birth_year])

    # Override the __str__() method to return out something meaningful
    def __str__(self):
        return self.user.username[/code]

Register it into the admin app by replacing the contents of
 &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_root/auth_lifecycle/admin.py</code>
with

[code language="python"]from django.contrib import admin
from .models import UserProfile

admin.site.register(UserProfile)[/code]

and then sync it to the database:

<ol>
   <li><code>source /home/myname/django_auth_lifecycle/djauth_venv/bin/activate</code></li>
   <li><code>cd /home/myname/django_auth_lifecycle/djauth_root/</code></li>
   <li><code>python manage.py <a href="https://docs.djangoproject.com/en/1.7/ref/django-admin/#django-admin-makemigrations">makemigrations</a></code></li>
   <li><code>python manage.py <a href="https://docs.djangoproject.com/en/1.7/ref/django-admin/#migrate-app-label-migrationname">migrate</a></code><a name="first_tests"></a></li>
</ol>

The same model with no validation:

[code language="python"]"""
Defines a single extra user-profile field for the user-authentication
lifecycle demo project: Birth year. There is no validation on this field.
"""
from django.contrib.auth.models import User
from django.db                  import models

class UserProfile(models.Model):
    """
    Extra information about a user: Birth year.

    ---NOTES---

    Useful related SQL:
    - `select id from auth_user where username <> 'admin';`
    - `select * from auth_lifecycle_userprofile where user_id=(x,x,...);`
    """
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User, related_name="profile")

    # The additional attributes we wish to include.
    birth_year = models.IntegerField(
        blank=True,
        verbose_name="Year you were born")

    # Override the __str__() method to return out something meaningful
    def __str__(self):
        return self.user.username[/code]<a name="test_utils"></a>

<h1>Utilities needed by future tests</h1>

There's <a href="/2014/10/24/fixtures_to_factories/#nothing_to_test">nothing to test yet</a>. However, we can already create some very useful utilities for future tests: creating test-users in bulk, logging a user in, finding specific text in the html, and debugging. This is also a good spot to place some more generic testing documentation.

The tests require <a href="https://factoryboy.readthedocs.org/en/latest/">Factory Boy</a> to create its demo data (instead of <a href="/2014/10/24/fixtures_to_factories/">creating it manually</a>). To <a href="https://factoryboy.readthedocs.org/en/latest/#download">install</a> it: <ol>
  <li><code>source /home/myname/django_auth_lifecycle/djauth_venv/bin/activate</code></li>
  <li><code>sudo /home/myname/django_auth_lifecycle/djauth_venv/bin/pip install factory_boy</code></li>
</ol><a name="test__utilities"></a>

Save the following as
 &nbsp; &nbsp; <code>/home/myname/django_auth_lifecycle/djauth_root/auth_lifecycle/test__utilities.py</code>

[code language="python"]"""
Utilities used by testing code throughout the authentication-lifecycle and
testing tutorial.

DEPENDS ON TEST:  *nothing* (must not depend on any test_*.py file)
DEPENDED ON TEST: test__profile.py

--- Generic information on running tests ---

To run a single test:
    1. source /home/myname/django_files/django_auth_lifecycle/djauth_venv/bin/activate
    2. cd /home/myname/django_files/django_auth_lifecycle/djauth_root/
    3. python -Wall manage.py test auth_lifecycle.test__file_name

To run all tests:
    python -Wall manage.py test auth_lifecycle

Running tests documentation:
- https://docs.djangoproject.com/en/1.7/topics/testing/overview/#running-tests

Information on '-Wall' is at the bottom of that same section. If the
output is too verbose, try it again without '-Wall'.

If a test fails because the test database cannot be created, grant your
database user creation privileges:
- http://dba.stackexchange.com/questions/33285/how-to-i-grant-a-user-account-permission-to-create-databases-in-postgresql

pylint auth_lifecycle.test__utilities > pylint_output.txt
pylint auth_lifecycle.test__view_birth_stats > pylint_output.txt
pylint auth_lifecycle.test__view_user_profile > pylint_output.txt
"""
from .models                    import MIN_BIRTH_YEAR
from auth_lifecycle.models      import UserProfile
from django.contrib.auth.models import User
from django.test                import TestCase
import factory

TEST_USER_COUNT = 5
"""The number of test users to create. Equal to `5`."""
TEST_PASSWORD = 'password123abc'
"""The password shared by all test users. Equal to `'password123abc'`."""

class UserProfileFactory(factory.django.DjangoModelFactory):
    """
    Creates `UserProfile`-s, where each user has a unique birth year,
    starting with <link to .models.MIN_BIRTH_YEAR>.

    *Warning*: Creating more than
        MAX_BIRTH_YEAR - MIN_BIRTH_YEAR
     users will cause a ValidationError.
    """
    #Uncommenting this line would allow you to directly create a
    #UserProfile, which would then automatically create a User.
    #- Docs: http://factoryboy.readthedocs.org/en/latest/reference.html#subfactory
    #user = factory.SubFactory('auth_lifecycle.test__utilities.UserFactory', profile=None)
    class Meta:
        model = UserProfile

    #factory.Sequence always starts at one. This starts it at
    #MIN_BIRTH_YEAR.
    #http://factoryboy.readthedocs.org/en/latest/reference.html#sequence
    #http://stackoverflow.com/questions/15402256/how-to-pass-in-a-starting-sequence-number-to-a-django-factoryboy-factory
    birth_year = factory.Sequence(lambda n: n + MIN_BIRTH_YEAR - 1)

class UserFactory(factory.django.DjangoModelFactory):
    """
    Creates `User`-s and its corresponding `UserProfile`-s. Each user has
    the same attributes, but with a unique sequence number, starting with
    one.

    See <link to TEST_PASSWORD>.
    """
    class Meta:
        model = User
    #Automatically create a profile when the User is created.
    #- Docs: http://factoryboy.readthedocs.org/en/latest/reference.html?highlight=subfactory#relatedfactory
    profile = factory.RelatedFactory(UserProfileFactory, 'user')

    username = factory.Sequence(lambda n: 'test_username{}'.format(n))
    first_name = factory.Sequence(lambda n: 'test_first_name{}'.format(n))
    last_name = factory.Sequence(lambda n: 'test_last_name{}'.format(n))
    email = factory.Sequence(lambda n: 'test_email{}@example.com'.format(n))

    #http://factoryboy.readthedocs.org/en/latest/reference.html#postgenerationmethodcall
    #See Django mention at the bottom of that documentation section.
    password = factory.PostGenerationMethodCall('set_password', TEST_PASSWORD)

def create_insert_test_users():
    """
    Insert <link to TEST_USER_COUNT> test users into the database. I don't
    understand why, but even though this is called for every test, via
    `setUp`, this does *not* create more than `TEST_USER_COUNT` users.
    Use the debugging statements to prove this.
    """

    #print('a User.objects.count()=' + str(User.objects.count()))

    #http://factoryboy.readthedocs.org/en/latest/reference.html?highlight=create#factory.create_batch
    UserFactory.create_batch(TEST_USER_COUNT)

    #print('b User.objects.count()=' + str(User.objects.count()))

def login_get_next_user(test_instance):
    """
    Log in the next test user, assert it succeeded, and return the `User`
    object.
    """
    test_instance.client.logout()

    test_user = UserFactory()
    #debug_test_user(test_user, prefix='Attempting to login:')

    did_login_succeed = test_instance.client.login(
        username=test_user.username,
        password=TEST_PASSWORD)
    test_instance.assertTrue(did_login_succeed)

    return  test_user

def assert_attr_val_in_content(
        test_instance, attribute_name, expected_value, page_content_str):
    """A specific attribute should be somewhere in the html."""
    #print('assert_attr_val_in_content: expected_value=' + expected_value)
    test_instance.assertTrue(str(expected_value) in page_content_str)

def debug_test_user(test_user, prefix=''):
    """
    Print all user attributes to standard out, except password.

    Parameters:
    - prefix: Defaults to `''`. If not the empty string, printed before
    the user information
    """
    if  prefix is not '':
        print(prefix)

    profile = test_user.profile
    print('test_user.id=' + str(test_user.id))
    print('   username=' + test_user.username + ', password=' + TEST_PASSWORD)
    print('   first_name=' + test_user.first_name + ', last_name=' + test_user.last_name)
    print('   email=' + test_user.email)
    print('   profile=' + str(profile))
    print('      profile.birth_year=' + str(profile.birth_year))[/code]<a name="run_tests"></a>

---

That's it for now.

In the next post, well implement and test the first of two views: The private user-profile page.

<i><code><b>[TOC: <a href="/2014/10/16/django_auth_tutorial/">one</a>, <a href="/2014/10/19/django_auth_tutorial_2/">two</a>, <a href="/2014/10/19/django_auth_testing_tutorial_3/">three</a>, <a href="/2014/12/29/django_auth_tutorial_4/">four</a>, <a href="/2015/01/26/django_auth_tutorial_5/">five</a>, <a href="/2015/02/12/django_auth_tutorial_6/">six</a>, <a href="/2015/02/13/django_auth_tutorial_7/">seven</a>, eight, nine, ten]</b></code></i>



<i>At this point, it would be a good idea to backup your files.</i>

<h1><i>...to be continued...</i></h1>


<i>(cue <a href="http://www.dramabutton.com/">cliffhanger segue music</a>)</i>
