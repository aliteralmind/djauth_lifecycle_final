	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.Overwrite;
	import  com.github.xbn.io.PlainTextFileUtil;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart07
 */
public class BuildPart07  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_07/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/07_forgot_my_password.html";
			String outputPath = postRoot + "temp_post_output/07_forgot_my_password__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");

		//Go
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";
			String registrationUrlsDotPy = PlainTextFileUtil.getText(alRoot +
				"registration/urls.py", "registrationUrlsDotPy").trim();
			String authFormResetSetPwdDotPy = PlainTextFileUtil.getText(alRoot +
				"registration/form_reset_set_new_pwd.py", "authFormResetSetPwdDotPy").trim();
			String alTmplReg = alRoot + "templates/registration/";
			String pswdReset3of4NewPwdForm = PlainTextFileUtil.getText(alTmplReg +
				"pwd_reset_3of4_new_pwd_form.html", "pswdReset3of4NewPwdForm").trim();
			String origPwdResetConfirmDotHtml = PlainTextFileUtil.getText(djangoRoot +
				"dalt07_venv/lib/python3.4/site-packages/django/contrib/admin/templates/registration/password_reset_confirm.html", "origPwdResetConfirmDotHtml").trim();

			String rendered = tmpl.
					fill("auth_form_reset_set_new_pwd_dot_py", authFormResetSetPwdDotPy).
					fill("registration_urls_dot_py", registrationUrlsDotPy).
					fill("pwd_reset_3of4_new_pwd_form_dot_html", pswdReset3of4NewPwdForm).
					fill("orig_password_reset_confirm_dot_html", origPwdResetConfirmDotHtml).
				getFilled();

			rendered = rendered.
				//Global:
					replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
					replace("\t", "   ").
				//Part specific:
					replace("dalt07_venv", "djauth_venv");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}
