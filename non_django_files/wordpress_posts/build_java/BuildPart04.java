	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.Overwrite;
	import  com.github.xbn.io.PlainTextFileUtil;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart04
 *
 */
public class BuildPart04  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_04/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/04_login_logout.html";
			String outputPath = postRoot + "temp_post_output/04_login_logout__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");

		//Go
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";
			String authRoot = alRoot + "registration/";
			String loginDotHtml = PlainTextFileUtil.getText(alRoot +
				"templates/registration/login.html", "loginDotHtml").trim();
			String authUrlsDotPy = PlainTextFileUtil.getText(authRoot +
				"urls.py", "authUrlsDotPy").trim();
			String authTestLoginBasicDotPy = PlainTextFileUtil.getText(authRoot +
				"test_login_basic.py", "authTestLoginBasicDotPy").trim();

			String rendered = tmpl.
					fill("login_dot_html", loginDotHtml).
					fill("auth_urls_dot_py", authUrlsDotPy).
					fill("auth_test_login_basic_dot_py", authTestLoginBasicDotPy).
				getFilled();

			rendered = rendered.
				//Global:
					replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
					replace("\t", "   ").
				//Part specific:
					replace("dalt04_venv", "djauth_venv");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}
