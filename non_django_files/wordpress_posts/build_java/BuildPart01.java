	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.PlainTextFileUtil;
	import  com.github.xbn.io.Overwrite;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart01
 */
public class BuildPart01  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_01/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/01_setup_model_test_utilities.html";
			String outputPath = postRoot + "temp_post_output/01_setup_model_test_utilities__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");

		//Go
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";

			String modelsDotPy = PlainTextFileUtil.getText(alRoot + "models.py", "modelsDotPy").trim();
			String adminDotPy = PlainTextFileUtil.getText(alRoot + "admin.py", "adminDotPy").trim();
			String modelsNoVDotPy = PlainTextFileUtil.getText(alRoot + "models_no_validation.py", "modelsNoVDotPy").
				trim();
			String testUtilsDotPy = PlainTextFileUtil.getText(alRoot + "test__utilities.py", "testUtilsDotPy").
				trim();

			String rendered = tmpl.
					fill("models_dot_py", modelsDotPy).
					fill("admin_dot_py", adminDotPy).
					fill("models_no_validation_dot_py", modelsNoVDotPy).
					fill("test__utilities_dot_py", testUtilsDotPy).
				getFilled();

			rendered = rendered.
				//Global:
					replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
					replace("\t", "   ").
				//Part specific:
					replace("dalt01_venv", "djauth_venv").
					replace("-R jeffy", "-R myname");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}