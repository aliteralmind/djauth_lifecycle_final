	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.Overwrite;
	import  com.github.xbn.io.PlainTextFileUtil;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart06
 */
public class BuildPart06  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_06/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/06_login_user_pass_length.html";
			String outputPath = postRoot + "temp_post_output/06_login_user_pass_length__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");

		//Go
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";
			String modelsDotPy = PlainTextFileUtil.getText(alRoot +
				"models.py", "modelsDotPy").trim();
			String loginDotHtml = PlainTextFileUtil.getText(alRoot +
				"templates/registration/login.html", "loginDotHtml").trim();
			String authFormResetPwdDotPy = PlainTextFileUtil.getText(alRoot +
				"registration/view_login.py", "authFormResetPwdDotPy").trim();
			String testUserPassLenVarsDotPy = PlainTextFileUtil.getText(alRoot +
				"registration/test_login_user_pass_len_vars.py", "testUserPassLenVarsDotPy").trim();
			String vldtUsrPassJs = PlainTextFileUtil.getText(djangoRoot +
				"djauth_root/static/js/validate_login_user_pass.js",
				"vldtUsrPassJs").trim();

			String rendered = tmpl.
					fill("models_dot_py", modelsDotPy).
					fill("login_dot_html", loginDotHtml).
					fill("validate_login_user_pass_dot_js", vldtUsrPassJs).
					fill("view_login_dot_py", authFormResetPwdDotPy).
					fill("test_login_user_pass_len_vars_dot_py", testUserPassLenVarsDotPy).
				getFilled();

			rendered = rendered.
				//Global:
					replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
					replace("\t", "   ").
				//Part specific:
					replace("dalt06_venv", "djauth_venv");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}
