	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.Overwrite;
	import  com.github.xbn.io.PlainTextFileUtil;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart02
 */
public class BuildPart02  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_02/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/02_profile_view.html";
			String outputPath = postRoot + "temp_post_output/02_profile_view__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");

		//Go
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";

			String viewProfileDotPy = PlainTextFileUtil.getText(alRoot + "view__profile.py", "viewProfileDotPy").
				trim();
			String userProfileDotHtml = PlainTextFileUtil.getText(alRoot + "templates/auth_lifecycle/user_profile.html", "userProfileDotHtml").
				trim();
			String testViewProfileDotPy = PlainTextFileUtil.getText(alRoot + "test__view_profile.py", "testViewProfileDotPy").
				trim();
			String dalUrlsDotPy = PlainTextFileUtil.getText(djangoRoot + "djauth_root/django_auth_lifecycle/urls.py", "dalUrlsDotPy").
				trim();
			String alUrlsDotPy = PlainTextFileUtil.getText(alRoot + "urls.py", "alUrlsDotPy").
				trim();

			String rendered = tmpl.
					fill("view__profile_dot_py", viewProfileDotPy).
					fill("user_profile_dot_html", userProfileDotHtml).
					fill("test__view_profile_dot_py", testViewProfileDotPy).
					fill("dal_urls_dot_py", dalUrlsDotPy).
					fill("al_urls_dot_py", alUrlsDotPy).
				getFilled();

			rendered = rendered.
			//Global
				replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
				replace("\t", "   ").
			//Part-specific
				replace("dalt02_venv", "djauth_venv");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}