	import  com.github.aliteralmind.templatefeather.FeatherTemplate;
	import  com.github.aliteralmind.templatefeather.GapCharConfig;
	import  com.github.aliteralmind.templatefeather.Resettable;
	import  com.github.xbn.io.Overwrite;
	import  com.github.xbn.io.PlainTextFileUtil;
/**
 * cd /home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/
 * java -classpath .:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/xbnjava-0.1.5.1.jar:/usr/lib/jvm/java-7-openjdk-i386/jre/lib/rt.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/commons-io-2.4.jar:/home/jeffy/django_files/djauth_lifecycle_tutorial/final/non_django_files/wordpress_posts/build_java/dependency_jars/templatefeather-0.1.1.3.jar BuildPart03
 */
public class BuildPart03  {
	public static final void main(String[] ignored)  {
		//Configuration
			String dalRoot = "/home/jeffy/django_files/djauth_lifecycle_tutorial/";
			String djangoRoot = dalRoot + "part_03/";
			String postRoot = dalRoot + "final/non_django_files/wordpress_posts/";
			String inputPath = postRoot + "templates/03_main_view.html";
			String outputPath = postRoot + "temp_post_output/03_main_view__built.html";

			GapCharConfig gapcfg = new GapCharConfig('@', '@',
				"__AT_SIGN_START__", "__AT_SIGN_END__");


		//Read in raw template
			String origText = PlainTextFileUtil.getText(inputPath, "origText");
			FeatherTemplate tmpl = new FeatherTemplate(origText, gapcfg,
				Resettable.NO, System.out);

		//Read in source code, one per gap
			String alRoot = djangoRoot + "djauth_root/auth_lifecycle/";

			String viewMainDotPy = PlainTextFileUtil.getText(alRoot + "view__main.py", "viewMainDotPy").
				trim();
			String mainPageDotHtml = PlainTextFileUtil.getText(alRoot + "templates/auth_lifecycle/main_page.html", "mainPageDotHtml").
				trim();
			String testViewMainDotPy = PlainTextFileUtil.getText(alRoot + "test__view_main.py", "testViewMainDotPy").
				trim();

		//Fill in gaps with read-in code

			//Example gap: @view__main_dot_py@

			String rendered = tmpl.
					fill("view__main_dot_py", viewMainDotPy).
					fill("main_page_dot_py", mainPageDotHtml).
					fill("test__view_main_dot_py", testViewMainDotPy).
				getFilled();

			rendered = rendered.
				//Global
					replace(djangoRoot, "/home/myname/django_auth_lifecycle/").
					replace("\t", "   ").
				//Part-specific
					replace("dalt03_venv", "djauth_venv");

			PlainTextFileUtil.openWriteClose(outputPath, "outputPath",
				Overwrite.YES, rendered);

			outputPath = outputPath.replace("/home/jeffy/", "Q:\\").replace("/", "\\");
			System.out.println("Output:\n" + outputPath);
	}
}