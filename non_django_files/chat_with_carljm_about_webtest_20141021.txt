http://django-irc-logs.com/2014/oct/23/#1629190

<aliteralmind>Is this an appropriate way to test a login page? My idea is to test a submission as described here (https://docs.djangoproject.com/en/1.7/topics/testing/tools/#overview-and-a-quick-example ), but then to also test the DOM of the rendered output, that it has a form element named "login_form",
<aliteralmind>and that the form has elements with the names "username", "password", and "remember_me", and two hidden elements named "next" and "csrfmiddlewaretoken". The goal is that the page is structured the way the code expects, without any restrictions on the content or format.
<key_>aliteralmind: selenium is one route you can use to test pages
<carljm>aliteralmind: for something that can write that type of test (while not being as heavyweight/slow as Selenium), you might look at WebTest: http://webtest.readthedocs.org/en/latest/
<key_>yes, there is
<aliteralmind>key_: Unfortunately, this is on a command-line only box. So I'm limited to the Django test server. Disappointing, because I'd like to see Selenium in action. Thanks, though.
<carljm>aliteralmind: (and if you're using Django, there's django-webtest to provide some convenient integration: https://pypi.python.org/pypi/django-webtest )
<carljm>aliteralmind: webtest is basically like the Django test client, but better. I write all my request/response tests using it.
<carljm>aliteralmind: alternatively, if you don't feel like switching test clients, you can also write that type of test by just using html5lib or lxml or BeautifulSoup (pick your preference) to parse the HTML response, and then asserting against that HTML structure
<carljm>(webtest just does the parsing part of that for you)
<aliteralmind>carljm: Hm. Will read up on that stuff. My only idea so far was to get the request from Django, and inspect the DOM with xml.etree.ElementTree. Never did any of this stuff before.
<aliteralmind>carljm: response, I mean.
<aliteralmind>key_ and carljm: Thanks.
<carljm>aliteralmind: Yes - with either of the approaches I discussed, that's what you'll be doing. WebTest just does the "parse to lxml/BeautifulSoup" part for you, and gives you a handy attribute for that on its response object
<carljm>aliteralmind: the more powerful thing that WebTest does is automatically parse form HTML, and allow you to "fill it out" and submit it back like a browser would
<carljm>aliteralmind: which means that if it's form markup you're testing, rather than making a bunch of assertions about the form (CSRF token present, correct fields present, etc), you can go one step above that and just test that it works when submitted.
<aliteralmind>carljm: That would be much better. Didn't think it was possible without Selenium. So WebTest-django is where I should start?
<aliteralmind>carljm: django-webtest it's called.
<carljm>aliteralmind: Yes, install django-webtest and follow its instructions. Then you'll want to look over the WebTest docs I linked to understand how to actually use its APIs for writing test assertions
<aliteralmind>carljm: Excellent. Will do. Lots of reading for tonight, and will actually start with it tomorrow. Thanks again.
